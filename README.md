dxarts461 : Read Me
========================
_DXARTS 461 (Winter 2023): Introduction to digital sound synthesis (Assignment Briefs)._

&nbsp;

&nbsp;

Installing
==========

Distributed via [DXARTS 461 Tutorials](https://gitlab.com/dxarts/classes/dxarts46x/dxarts461a_wi2x.quark).


Feedback and Bug Reports
========================

Known issues are logged at
[GitLab](https://gitlab.com/dxarts/classes/dxarts46x/dxarts461a_wi23_briefs.quark/-/issues).

&nbsp;

&nbsp;



List of Changes
---------------

Version 23.10.0
* Winter 2023: week 10
    * assignment-5: solution

Version 23.7.0
* Winter 2023: week 7
    * assignment-3: solution
    * assignment-5: brief
    * final: brief

Version 23.5.0
* Winter 2023: week 5
    * assignment-2: solution
    * assignment-3: brief
    * midterm: shepardsTone --> shepardRissetGliss

Version 23.4.0
* Winter 2023: week 4
    * midterm: brief

Version 23.3.1
* Winter 2023: week 3
    * fix: accommodate OscUGens init(s) found in SC ver 3.13.0-rc1

Version 23.3.0
* Winter 2023: week 3
    * assignment-1: solution
    * assignment-2: brief

Version 23.0.0
* Winter 2023: week 0
    * assignment-1: brief


&nbsp;

&nbsp;

Authors
=======

&nbsp;

Copyright Joseph Anderson and the DXARTS Community, 2020-2023.

[Department of Digital Arts and Experimental Media (DXARTS)](https://dxarts.washington.edu/)
University of Washington

&nbsp;


Contributors
------------

Version 23.4.0 - 23.10.0
*  Joseph Anderson (@joslloand)

Version 23.3.1
*  Joseph Anderson (@joslloand)
*  Wei Yang (@wyang03)

Version 23.3.0
*  Joseph Anderson (@joslloand)

Version 23.0.0
*  Joseph Anderson (@joslloand)
*  Wei Yang (@wyang03)
*  James Wenlock (@wenloj)


Contribute
----------

As part of the wider SuperCollider community codebase, contributors are encouraged to observe the published SuperCollider guidelines found [here](https://github.com/supercollider/supercollider#contribute).


License
=======

The [DXARTS 461 Tutorials](https://gitlab.com/dxarts/classes/dxarts46x/dxarts461a_wi2x.quark) is free software available under [Version 3 of the GNU General Public License](https://www.gnu.org/licenses/gpl-3.0.en.html). See [LICENSE](LICENSE) for details.
