/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

Checks output of function
*/

({ |main, fileName, threshold = (-60.dbamp)|
	var thisOutput, correctOutput, fileExtension, result, outputPath;

	fileExtension = ".wav";
	outputPath = main.projectPath +/+ "soundOut" +/+ fileName ++ fileExtension;

	File.exists(outputPath).if({
		thisOutput = Signal.read(main.projectPath +/+  "soundOut" +/+ fileName ++ fileExtension);
		correctOutput = Signal.read(main.projectPath +/+  "soundIn" +/+ fileName ++ "-correct" ++ fileExtension);
		result = (thisOutput - correctOutput).rms;

		if (result <= threshold, {
			"correct!".postln;
		}, {
			"incorrect!".postln;
		})
	})
})
