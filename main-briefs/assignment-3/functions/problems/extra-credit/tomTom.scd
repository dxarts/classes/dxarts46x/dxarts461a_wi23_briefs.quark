/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
Create a tom tom sound additively using amSh2Drum and the following arguments

main: object containing project information
freq: fundamental frequency of tom
start: starttime of drum
dur: duration of drum
gain: volume of drum in db
ris: amount of time drum takes to reach full volume (fraction of duration)
curve: curve of amplitude envelope
ratios: array of ratios for noise bands

Gains, panAngle, as along q and modIndex envelope parameters for each noise band generated should be randomized to taste. Your tom tom should go from noisy to sinusoidal.

*/

({ |main, freq = 880, start = 0, dur = 1, gain = -6, ris = 0.01, curve - 6, ratios = ([0.56, 1, 1.57, 2.1, 1.32])|

	// YOUR CODE HERE
	// ---

	// ---
})
