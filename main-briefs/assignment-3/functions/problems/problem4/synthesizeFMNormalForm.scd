/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
----------------------
===== PROBLEM 4 =====
----------------------

Synthesize a bell-like spectra in normal form using fm synthesis, the first two partials of the Germantown Bell, and the following arguments

  main: object containing project contents
  start: starttime of bell playback
  dur: total duration of bell
  gain: gain of bell
  ris: amount of time tone takes to reach maximum volume (fraction of duration)
  curve: curve of amplitude envelope
  modIndex: index of modulation
  freq0: frequency of partial 0
  n: spectral family

Your c:m ratio should be calculated using main.functions[\cmRatiosFunc]
You should use main.synthDefs[\fmPercPan] to synthesize your fm spectra

EXPECTED OUTPUT
---------------

PROBLEM 4
---------
start time 0
nextOSCPacket 0
nextOSCPacket 0
nextOSCPacket 2
nextOSCPacket 2.2
nextOSCPacket 2.2
problem-4 successfully rendered
correct!
---------

HINTS:
See 05. FM Modulation & Spectral Design

The frequencies of the first two spectral components of the Germantown A3# bell:
f0 -> 116.0
f1 -> 232.4

We synthesise FM spectra by choosing a ratio n, which specifies the musical interval between the first and second spectral components, f0 and f1, of the synthesized spectrum. This ratio is what defines an FM Spectral Family, and is found using the following equation:

n = f1 / f0;

*/

({ |main, start = 0, dur = 2, gain = -6, ris = 0.05, curve = -7, freq0 = 220, modIndex = 1, n = (232.4/116.0)|
	var cm;

	cm = main.functions[\cmRatioFunc].value(n: n, p: 0);

	// YOUR CODE HERE
	// ---

	// ---
})