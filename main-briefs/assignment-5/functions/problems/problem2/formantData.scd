/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
-----------------
=== PROBLEM 2 ===
-----------------

Complete this function so that it returns a dictionary containing the format data for the female vowels found in table 7.3 on pg.231 of the Dodge and Jerse.

Dodge and Jerse have not supplied relevant Qs for each vowel. Use these values:

a: \qs, [ 2.0, 3.0, 1.0, 3.0, 3.0 ] // quality factor
e: \qs, [ 2.0, 1.0, 4.0, 2.0, 3.0 ] // quality factor
iy: \qs, [1.0, 1.0, 2.0, 3.0, 1.0 ] // quality factor
o: \qs, [1.0, 3.0, 4.0, 3.0, 3.0 ] // quality factor
oo: \qs, [2.0, 3.0, 1.0, 1.0, 3.0 ] // quality factor


Attribution:

Dodge, Charles, and Jerse, Thomas A. Computer Music : Synthesis, Composition, and Performance. 2nd ed., Schirmer Books ; Prentice Hall International, 1997.

Tables 7.2, 7.3 (pp. 230, 231)

[1] Vowel Formats (freq & gain)



Hint: see also:

07a-synchronous-granular-synthesis
   02-sgs-bln-additive-synthesis
      functions
         data

*/

({ |main|
	var a, e, iy, o, oo;

	a = [
		\freqs, [650, 1100, 2860, 3300, 4500], // [1] freqs, in Hz
		\gains, [0, -8, -14, -12, -19], // [1] gains, in dB
		\qs, [ 2.0, 3.0, 1.0, 3.0, 3.0 ] // quality factor
	].asDict;

	iy = [
		\freqs, [330, 2000, 2800, 3650, 5000], // [1] freqs, in Hz
		\gains, [0, -14, -11, -10, -19], // [1] gains, in dB
		\qs, [1.0, 1.0, 2.0, 3.0, 1.0 ] // quality factor
	].asDict;

	// YOUR CODE HERE
	// ---


	// ---

	[
		\a, a,
		\e, e,
		\iy, iy,
		\o, o,
		\oo, oo
	].asDict
})
