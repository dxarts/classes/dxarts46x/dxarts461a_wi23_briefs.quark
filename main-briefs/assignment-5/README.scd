/*
Title: assignment-5

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
------------------------
===== ASSIGNMENT 5 =====
------------------------
Experiments with Granular Synthesis

The fifth assignment is intended to familiarize you with granular synthesis techniques

----------------------------
=====HOW TO DO HOMEWORK=====
----------------------------

Assignments require you to complete functions and synthDefs contained in the given assignment folder. Problems will come in the form of .scd files located in two problems/ directories contained respectively in the functions/ and synthDefs/ directories.


Below is the assignment-5 project file hierarchy. The problems/ directories are emphasized

assignment-5/

____assignment-5.scd

____functions/

________DO-NOT-EDIT/

________problems/ <- PROBLEMS [1, 2, 5, extra-credit] ARE LOCATED HERE

____README.scd

____images

____soundIn/

____soundOut/

____uGenGraphs/

________DO-NOT-EDIT/

________problems/ <- PROBLEMS [3, 4, extra-credit] ARE LOCATED HERE

Upon opening a .scd file in the problems/ directory, you will find a comment header with specifications for that problem. Places for student code are labeled with the following syntax

// YOUR CODE HERE
// ---

// ---

Your answer is to be written between the dashed lines.

Some problems will come in the form of a folder rather than an .scd. If this is the case it is expected that you modify each .scd file(s) contained inside of the folder.

You might have notice there are two folders named DO-NOT-EDIT. These directories, if present, contain code otherwise used to evaluate your answers. The files in these folders are not to be modified!

Your homework will be evaluated by running the code block inside of assignment-5.scd.

Appropriate output for each problem section will either be output in the post window or a soundfile, and is specified within the individual problem file.

You can render specific problems by changing the booleans inside of assignment-5.scd

*/
