/*
Title: assignment-1

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
------------------------
===== ASSIGNMENT 1 =====
------------------------
Functions, Arrays, Iteration, and the Main Paradigm

This first assignment is intended to help you become familiar with sclang, the SuperCollider language, and SC-IDE, SuperCollider's Integrated Development Environment.
Assigned problems are focused on working with functions, arrays, exploring iteration, as well as the using main paradigm and interacting with the CTK

----------------------------
=====HOW TO DO HOMEWORK=====
----------------------------

Assignments require you to complete functions and synthDefs contained in the given assignment folder. Problems will come in the form of .scd files located in two problems/ directories contained respectively in the functions/ and synthDefs/ directories.

ASSIGNMENT 1 consists of eight problems and one extra credit.

Below is the assignment-1 project file hierarchy. The problems/ directories are emphasized

assignment-1/

____assignment-1.scd

____functions/

________DO-NOT-EDIT/

________problems/ <- PROBLEMS [1a, 1b, 1c, 2a, 2b, 3, 5, extraCredit] ARE LOCATED HERE

____README.scd

____soundIn/

____soundOut/ (This will be generated once the code is rendered)

____uGenGraphs/

________problems/ <- PROBLEMS 4 IS LOCATED HERE

Upon opening a .scd file in the problems/ directory, you will find a comment header with specifications for that problem. Places for student code are labeled with the following syntax

// YOUR CODE HERE
// ---

// ---

Your answer is to be written between the dashed lines.

Some problems will come in the form of a folder rather than an .scd. If this is the case it is expected that you modify each .scd file(s) contained inside of the folder.

You might have notice there are two folders named DO-NOT-EDIT. These directories, if present, contain code otherwise used to evaluate your answers. The files in these folders are not to be modified!

Your homework will be evaluated by running the code block inside of assignment-1.scd.

Appropriate output for each problem section will either be output in the post window or a soundfile, and is specified within the individual problem.scd file.

IMPORTANT: You do not need to not change assignment-1.scd!
*/
