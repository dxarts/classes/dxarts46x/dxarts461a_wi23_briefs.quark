/*
Title: assignment-1

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
---------------------
===== PROBLEM 1b =====
---------------------
Iterate through the given array and post each value. Then post the sum of these values.

OUTPUT ->

PROBLEM 1b
---------------
46
3256
2
45
9
123
sum = 3481
---------------

HINTS:
Have a look at String and its methods -post and -postln
Have a look at Array and the .do method
review: 02a. sclang, Functions, Arrays, Iteration, etc.
[1, 2, 3].sum -> 6

PLEASE NOTE:
You do not need to post the problem number and dashed lines. This is done in assignment-1.scd.
*/
({
	var array;

	// array you will iterate through
	array = [46, 3256, 2, 45, 9, 123];

	// YOUR CODE HERE
	// ---

	// ---
})
