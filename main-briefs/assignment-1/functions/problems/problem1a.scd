/*
Title: assignment-1

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
---------------------
===== PROBLEM 1 =====
---------------------

In the post window print "Hello World!"

EXAMPLE OUTPUT ->

PROBLEM 1a
---------------
Hello World
---------------

HINTS:
Have a look at String, and its methods -post and -postln,

PLEASE NOTE:
You do not need to post the problem number and dashed lines. This is done in assignment-1.scd.
*/
({
	// YOUR CODE HERE
	// ---


	// ---
})
