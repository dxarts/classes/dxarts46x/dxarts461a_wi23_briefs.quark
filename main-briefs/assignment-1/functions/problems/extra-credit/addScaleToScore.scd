/*
Title: assignment-1

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
------------------------
===== Extra Credit =====
------------------------
Using functions and synthDefs created in problems 5 -> 8, modify addScaleToScore to synthesize a scale for an equal temperament tuning system

You must meet the following requirements:
    1) Use the following functions: addMySinOsc, calcHarmonicFreqs, createScale
    2) Use the following synthDefs: mySinOsc
    3) Each scale tone must be sequential and distinct
    4) Each scale tone should be be synthesized with a specified number of harmonics
    5) Each harmonic should have a random gain
    6) addScaleToScore takes and properly use the following arguments,
        main: Dictionary containing global variables, functions, and synthDefs
        (you will use this to access your functions)
        start: starttime of scale playback
        dur: duration of each note
        numHarms: number of harmonics synthesized per scale tone
        baseInt: base interval
        numDivs: number of divisions of the baseInterval
        frequency: fundamental frequency (in Hertz)
        maxGain: maximum gain of each harmonic
        minGain: minimum gain of each harmonic

EXAMPLE OUTPUT ->

PROBLEM extra-credit
---------

start time 0
nextOSCPacket 0
...
nextOSCPacket 7.4
problem-extra-credit successfully rendered
---------


HINTS:
See 02c. Composer's Toolkit, Iteration, etc.
rrand(min, max) outputs a number between a minimum and maximum value
[1, 2, 3, 4, 5] * 2 -> [2, 4, 6, 8, 10]

Review:

02c-composers-toolkit-iteration-etc/

____01-ctk-and-the-main-paradigm


PLEASE NOTE:
Score creation and rendering is done in assignment-1.scd
You do not need to create or render a score in this function

CHANGE BASE INTERVAL AND NUMDIVISIONS
*/
({ |main, start = 0, dur = 1, numHarms = 4, frequency = 220.0, maxGain = -12, minGain = -40, baseInt = 2, numDivs = 12|
	var scale, harmonics;

	// YOUR CODE HERE
	// ---

	// ---
})
