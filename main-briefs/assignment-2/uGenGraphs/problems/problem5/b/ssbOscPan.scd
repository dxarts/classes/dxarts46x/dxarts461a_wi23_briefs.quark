/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
----------------------
===== PROBLEM 5b =====
----------------------

Modify the following uGenGraph so as to a synthesize a frequency shifted tone

EXAMPLE OUTPUT ->

PROBLEM 5b
---------
start time 0
nextOSCPacket 0
nextOSCPacket 0
nextOSCPacket 9.98843e-08
nextOSCPacket 1.99769e-07
nextOSCPacket 2.99886e-07
nextOSCPacket 3.9977e-07
nextOSCPacket 4.99887e-07
nextOSCPacket 5.99772e-07
nextOSCPacket 6.99889e-07
nextOSCPacket 2
nextOSCPacket 4
nextOSCPacket 6
nextOSCPacket 8
nextOSCPacket 10
nextOSCPacket 12
nextOSCPacket 14
nextOSCPacket 14.1
nextOSCPacket 14.2
nextOSCPacket 14.4
nextOSCPacket 14.4
problem-5b successfully rendered
---------

HINT: See 04b. RM(DSB) & SSB Modulation

*/
({ |main|
	{ |dur, gain = -12.0, ris = 0.1, dec = 0.1, freqCar  = 0.0, freqMod = 440.0, panAngle = 0.0, bufferCos = 0, bufferSin = 1|
		// YOUR CODE HERE

		// ---
	}
})
