/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
----------------------
===== PROBLEM 5a =====
----------------------

Output the real and imaginary buffers as required for SSB synthesis

EXAMPLE OUTPUT ->

PROBLEM 5a
---------
Quad Table
  real: a CtkBuffer
  imag: a CtkBuffer
---------

HINT: See 04b. RM(DSB) & SSB Modulation
*/

({ |main, size, partialGains|

	// YOUR CODE HERE
	// ---
	Array.new  // replace this!
	// ---
})
