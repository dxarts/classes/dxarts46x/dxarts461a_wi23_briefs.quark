/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

({ |main, problemNum, testFunc, condition, checkOutput = true, threshold = (-60.dbamp)|
	var fileName;
	main.score = CtkScore.new;
	fileName = "problem-" ++ problemNum;
	testFunc.value(main);
	("PROBLEM" + problemNum).postln;
	"---------".postln;
	main.render(
		fileName: fileName,
		openSF: true,
		action: {
			if (checkOutput, {
				main.functions[\checkOutput].value(main, fileName, threshold);
			});
			"---------".postln;
			"".postln;
			condition.test_(true).signal();
		},
	);
	"".postln;
})
