/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*

--------------------
=== EXTRA-CREDIT ===
--------------------

Alter the function created in problem 5 to synthesize time formant synthesis subtractively

HINT:

1) Design the \bpSawVibrato UGen graph
2) Call the \bpSawVibrato here...

*/

({|main, start, dur, gain = -12.0, ris = 0.1, dec = 0.1, freq = 440.0, v1 = \a, v2 = \oo, v1Sus = 0.1, v2Sus = 0.1, morphCurve = 0, vibRate = 4, vibAmt = 10|

	// YOUR CODE HERE
	// ---
	var numForms;
	var thisSynthDef;
	var formantData;
	var morphDur;
	var v1FormantData, v2FormantData;
	var createEnv;

	formantData = main.functions[\formantData].value();

	v1FormantData = formantData.at(v1);
	v2FormantData = formantData.at(v2);

	// catch - minimum number of items!

	numForms = [v1FormantData, v2FormantData].collect({ |thisFormantData|
		thisFormantData.values.collect({ |item|
			item.size
		}).minItem;
	}).minItem;

	// set synthDef, before loop
	thisSynthDef = main.synthDefs[\bpSawVibrato];

	createEnv = { |type = \freqs, i|
		var levels;
		levels = [
			v1FormantData[type].at(i),
			v1FormantData[type].at(i),
			v2FormantData[type].at(i),
			v2FormantData[type].at(i),
		];
		if (type == \gains, {
			levels = levels + gain;
		});

		CtkControl.env(
			Env.new(
				levels,
				[v1Sus, (1.0 - (v1Sus + v2Sus)), v2Sus],
				[0, morphCurve, 0]
			),
			timeScale: dur
		);
	};

	// iterate through the partialData dictionary to generate individual notes to add to the score
	// nothing fancy...
	numForms.do({ |i|
		var thisFormNote;
		var thisFormFreqEnv, thisFormGainEnv, thisFormQEnv;
		var createEnvArray;

		thisFormFreqEnv = createEnv.value(\freqs, i);
		thisFormGainEnv = createEnv.value(\gains, i);
		thisFormQEnv = createEnv.value(\qs, i);  // q

		// create a note for each formant... SGS: pitch
		thisFormNote = thisSynthDef.note(
			starttime: start, duration: dur
		)
		.dur_(dur)
		.gain_(thisFormGainEnv)
		.ris_(ris)
		.dec_(dec)
		.freq_(freq)
		.formFreq_(thisFormFreqEnv)  // formant
		.q_(thisFormQEnv)
		.vibRate_(vibRate)
		.vibAmt_(vibAmt);

		// then and add notes for each formant to the score
		main.score.add(thisFormNote);
	})
	// ---
})
