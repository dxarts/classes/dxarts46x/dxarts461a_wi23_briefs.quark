/*
Title: assignment-5
Name: YOUR NAME HERE

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Evaluate this code block to render.
*/
(
var main, condition;
var runProblem1, runProblem2, runProblem3, runProblem4, runProblem5, runExtraCredit;


// -------------------------------------
// Booleans indicating problem execution
// Use these to render specific problems
runProblem1 = false;
runProblem2 = false;
runProblem3 = false;
runProblem4 = false;
runProblem5 = false;
runExtraCredit = false;
// -------------------------------------


// set up...
main = MainParadigm("".resolveRelative, s, true);
condition = Condition.new;

// .. and go!
s.waitForBoot({

	// Problem 1
	if (runProblem1, {
		main.functions[\checkProblem].value(
			main: main,
			problemNum: "1",
			condition: condition,
			testFunc: { |main|
				main.functions[\bassoonScale].value(main)
			},
			checkOutput: true
		);
		condition.test_(false);
		condition.wait;
	});

	// Problem 2
	if (runProblem2, {
		main.functions[\checkProblem].value(
			main: main,
			problemNum: "2",
			condition: condition,
			testFunc: { |main|
				var vowels = [ \a, \e, \iy, \o, \oo, ];
				vowels.do({ |thisVowel, i|
					var thisStart, dur, freq;
					dur = 2;
					freq = 220;
					thisStart = (i * dur);
					main.functions[\additiveFormantFunc].value(
						main: main,
						start: thisStart,
						dur: dur,
						freq: freq,
						formantData: main.functions[\formantData].value()[thisVowel],
					);
				});
			},
			checkOutput: true
		);
		condition.test_(false);
		condition.wait;
	});

	// Problem 3
	if (runProblem3, {
		main.functions[\checkProblem].value(
			main: main,
			problemNum: "3",
			condition: condition,
			testFunc: { |main|
				main.score.add(
					main.synthDefs[\ssSGSVibrato].note(
						starttime: 0,
						duration: 2,
					)
					.dur_(2)
					.vibRate_(3)
					.vibAmt_(10)
				);
			},
			checkOutput: true
		);
		condition.test_(false);
		condition.wait;
	});

	// Problem 4
	if (runProblem4, {
		main.functions[\checkProblem].value(
			main: main,
			problemNum: "4",
			condition: condition,
			testFunc: { |main|
				var formData;
				formData = main.functions[\formantData].value()[\oo];
				main.functions[\additiveFormantFunc].value(
					main: main,
					formantData: formData,
					start: 0,
					dur: 2,
					freq: 220,
					synthDef: 'ssSGSAGS',
					gain: -12,
					ris: 0.05
				);
			},
			checkOutput: false
		);
		condition.test_(false);
		condition.wait;
	});

	// Problem 5
	if (runProblem5, {
		main.functions[\checkProblem].value(
			main: main,
			problemNum: "5",

			condition: condition,
			testFunc: { |main|
				main.functions[\additiveFormantMorph].value(
					main: main,
					start: 0,
					dur: 2
				);
			},
			checkOutput: true
		);
		condition.test_(false);
		condition.wait;
	});

	// Extra-Credit
	if (runExtraCredit, {
		main.functions[\checkProblem].value(
			main: main,
			problemNum: "extra-credit",
			condition: condition,
			testFunc: { |main|
				main.functions[\additiveFormantMorphEC].value(
					main: main,
					start: 0,
					dur: 2,
					gain: 4.0
				);
			},
			checkOutput: true
		);
		condition.test_(false);
		condition.wait;
	});
});
)
