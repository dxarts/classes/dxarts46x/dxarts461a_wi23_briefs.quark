/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
-----------------
=== PROBLEM 3 ===
-----------------

Modify the ssSGSVibrato to add vibrato to grain frequeny using a Sinusoidal LFO, via SinOsc.kr

Your vibrato should utilize the following arguments
  vibRate -> The rate of vibrato (in hertz)
  vibAmt -> The amount of vibrato (+- specified frequency)

HINT:

Vibrato is low frequency FM. See Dodge & Jerse chapter 4.8C and FIGURE 4.19 (pp 94-95)

*/

({ |main|
	{| dur, gain = -12.0, ris = 0.1, dec = 0.1, freq = 440.0, formFreq = 1760.0, q = 1.0, panAngle = 0.0, vibRate = 0.2, vibAmt = 1|

		// variables
		var bus;          // var to specify output bus
		var out;     // vars assigned to audio signals
		var amp;  // a few vars for synthesis
		var ampEnv, env;       // vars for envelope signal
		var grainDur, grainFreq, envFreq, wavFreq;
		var trigger;
		var granSig;
		var vibrato;


		// assign values
		bus = 0;          // first output

		// calcs
		amp = gain.dbamp; // convert from gain in dB to linear amplitude scale
		grainFreq = freq;
		envFreq = q.reciprocal * (formFreq/2);
		wavFreq = formFreq;
		grainDur = envFreq.reciprocal;

		// the amplitude envelope
		env = Env.linen(ris, 1.0 - (ris + dec), dec);

		// the UGen that synthesises the envelope
		ampEnv = EnvGen.kr(env, timeScale: dur);

		// YOUR CODE HERE
		// ---
		vibrato = SinOsc.kr(vibRate, mul: vibAmt);

		// granular (grain frequency) trigger
		trigger = Impulse.ar(grainFreq + vibrato);
		// ---

		// granular synthesis
		granSig = GrainSin.ar(trigger: trigger, dur: grainDur, freq: wavFreq);

		// apply the amplitude envelope
		granSig = amp * ampEnv * granSig;

		// expand to two channels - panning
		out = main.functions[\sinCosPanLaw].value(panAngle) * granSig;  // <-- Panning happens here!

		// out!!
		Out.ar(bus, out)
	}
})
