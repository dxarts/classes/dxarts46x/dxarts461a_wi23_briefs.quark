/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

An empty uGenGraph
*/

/*
-----------------
=== PROBLEM 4 ===
-----------------

Modify the ssSGSAGS so that it Asyncronous Granular Synthesis (AGS) and Syncronous Granular Synthesis (SGS) to get "airy" attacks and "sinusoidal" decays. ssSGSAGS should properly utilize the following arguments

dur -> duration of note
gain -> gain of note
ris -> amount of time amplitude envelope takes to reach full volume (fraction of duration)
dec -> amount of time amplitude envelope takes to reach -inf db
freq -> fundamental frequency
formFreq -> formant frequency
q -> formant quality factor
noiseDur -> duration of (AGS) noise
mixCurve -> curve of envelope modulating between AGS and SGS
density -> AGS density
panAngle -> panning angle


You should use Env to modulate between the two techniques.

*/

({ |main|
	{| dur, gain = -6.0, ris = 0.005, dec = 0.6, freq = 440.0, formFreq = 1760.0, q = 1.0, noiseDur = 0.8, mixCurve = -2, density = 1, panAngle = 0.0|

		// variables
		var bus;          // var to specify output bus
		var out;     // vars assigned to audio signals
		var amp;  // a few vars for synthesis
		var ampEnv, env;       // vars for envelope signal
		var gsMixEnv;
		var grainDur, sgsGrainFreq, agsGrainFreq, envFreq, wavFreq;
		var sgsTrigger, agsTrigger;
		var granSigAGS, granSigSGS;

		// assign values
		bus = 0;          // first output

		// calcs
		amp = gain.dbamp; // convert from gain in dB to linear amplitude scale
		sgsGrainFreq = freq;
		agsGrainFreq = density * q.reciprocal * formFreq;
		envFreq = q.reciprocal * (formFreq/2);
		wavFreq = formFreq;
		grainDur = envFreq.reciprocal;

		// AGS & SGS mix envelope
		gsMixEnv = EnvGen.kr(
			Env.new([1.0, 0.0, 0.0], [noiseDur, 1.0 - noiseDur], mixCurve),
			timeScale: dur
		);

		// YOUR CODE HERE
		// ---

		// the amplitude envelope
		env = Env.linen(ris, 1.0 - (ris + dec), dec);

		// the UGen that synthesises the envelope
		ampEnv = EnvGen.kr(env, timeScale: dur);

		// granular (grain frequency) trigger
		sgsTrigger = Impulse.ar(sgsGrainFreq);
		agsTrigger = Dust.ar(agsGrainFreq);

		// granular synthesis
		granSigSGS = GrainSin.ar(trigger: sgsTrigger, dur: grainDur, freq: wavFreq);
		granSigAGS = GrainSin.ar(trigger: agsTrigger, dur: grainDur, freq: wavFreq);
		// ---

		// mix AGS and SGS
		out = (granSigAGS * (gsMixEnv * pi / 2).sin) + ((gsMixEnv * pi / 2).cos * granSigSGS);

		// envelope
		out = out * amp * ampEnv;

		// expand to two channels - panning
		out = main.functions[\sinCosPanLaw].value(panAngle) * out;  // <-- Panning happens here!

		// out!!
		Out.ar(bus, out)
	}
})

