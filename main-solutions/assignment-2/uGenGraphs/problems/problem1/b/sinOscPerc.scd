/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
----------------------
===== PROBLEM 1b =====
----------------------

Modify the following synthdef so that its amplitude is modulated by Env.perc using the following parameters
  ris: amount of time envelope takes to reach maximum gain (fraction of duration)
  dur: duration of envelope
  curve: decay curve

EXAMPLE OUTPUT ->

PROBLEM 1b
---------
start time 0
nextOSCPacket 0
nextOSCPacket 0
nextOSCPacket 2
nextOSCPacket 2.2
nextOSCPacket 2.2
problem-1b successfully rendered
correct!
---------
*/


({ |main|
	{ |dur = 2, gain = -6, freq = 440, panAngle = 0, ris = 0.1, curve = -7|
		// variables
		var bus;          // var to specify output bus
		var osc, out;     // vars assigned to audio signals
		var amp, phase;  // a few vars for synthesis
		var ampEnv, env;       // vars for envelope signal

		// assign values
		bus = 0;          // first output
		phase = 0;        // phase of oscillator

		// calcs
		amp = gain.dbamp; // convert from gain in dB to linear amplitude scale

		// YOUR CODE HERE
		// ---
		env = Env.perc(ris, (1.0 - ris), curve: curve);

		ampEnv = EnvGen.kr(env, timeScale: dur);
		// ---

		// the oscillator
		osc = SinOsc.ar( // SinOsc UGen
			freq,        // 1st arg of SinOsc: freq (from synthDef argument)
			phase,       // 2nd arg of SinOsc: (set to a default value)
			amp          // 3rd arg of SinOsc: (set to a default value)
		);

		// rescale osc, by multiplying by ampEnv
		osc = ampEnv * osc;

		// expand to two channels
		out = osc * main.functions[\sinCosPanLaw].value(panAngle);

		// out!!
		Out.ar(         // Out UGen
			bus,        // 1st arg of Out: (set to a default value - the 1st output)
			out         // 2nd arg of Out: (two channels)
		)
	}
})
