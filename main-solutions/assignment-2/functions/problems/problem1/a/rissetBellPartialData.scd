/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
---------------------
===== PROBLEM 1a =====
---------------------
Using the table found in 03c. Measured Complex Waveforms, complete this function so that returns a dictionary containing the gains, durations, beats, and ratios needed to additively reproduce the Risset Bell.

NOTE:
In the table the +1 and +1.7 refer to differences in frequency, aka beats

EXAMPLE OUTPUT ->

Your data will be displayed in the post window in the following format

PROBLEM 1a
---------
Risset Bell
  durs: [ YOUR DATA ]
  beats: [ YOUR DATA  ]
  ratios: [ YOUR DATA  ]
  gains: [ YOUR DATA ]
---------
*/

({ |main|
	var gains, ratios, durs, beats;

	// Defines variables as empty arrays. You will need to overwrite these.
	gains = [];
	ratios = [];
	durs = [];
	beats = [];

	// YOUR CODE HERE
	// ---
	gains = [0.0, -3.5, 0.0, 5.1, 8.5, 4.5, 3.3, 2.5, 2.5, 0.0, 2.5];  // [ 1 ] gains, in dB

	// this data is "made up!"
	ratios = [0.56, 0.56, 0.92, 0.92, 1.19, 1.7, 2.0, 2.74, 3.0, 3.76, 4.07]; // stretched tuning ratios for each partial
	durs = [1.0, 0.9, 0.65, 0.55, 0.325, 0.35, 0.25, 0.2, 0.15, 0.1, 0.075];  // seconds

	beats = Array.fill(durs.size, {0});
	beats[1] = 1.0;
	beats[3] = 1.7;
	// ---

	// collection into a dictionay
	[
		\gains, gains,
		\ratios, ratios,
		\durs, durs,
		\beats, beats
	].asDict
})
