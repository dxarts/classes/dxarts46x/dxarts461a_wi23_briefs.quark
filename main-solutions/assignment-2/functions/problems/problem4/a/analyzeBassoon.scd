/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
----------------------
===== PROBLEM 4a =====
----------------------

Modify analyzeBassoon to analyze data gains for "numHarms" number of partials. Normalized gains, rounded to the nearest decibel. Threshold out any gains below mindb decibels. (Set these to -inf.) Discard DC.

Return the result as illustrated:

EXAMPLE OUTPUT ->

PROBLEM 4a
---------
Bassoon Analysis
  numHarms: 25
  gains: [ -18.0, 0.0, -21.0, -19.0, -26.0, -17.0, -26.0, -42.0, -32.0, -47.0, -44.0, -42.0, -48.0, -46.0, -53.0, -52.0, -55.0, -44.0, -55.0, -inf, -inf, -inf, -58.0, -inf, -inf ]
---------

HINTS:
see 03c-Measured-Complex-Waveforms

*/

({ |main, numHarms = 30|
	var size, freq, skipTime, soundFile, waveform, path, magnitudes, complex;
	var mindb, cosTable, rfftsize, gains;

		size = 4096;  // target waveform size
		skipTime = 1.5;  // in seconds
		freq = 220.0;  // estimated frequency of waveform @ 37.5 seconds, in Hz
		path = main.projectPath +/+ "soundIn" +/+ "bassoon-A3.aif";
		mindb = -60;


		// use SoundFile class to query sampleRate
		soundFile = SoundFile.new(path);

		// read and plot
		waveform = Signal.readWave(path, size, (soundFile.sampleRate * skipTime).asInteger, freq: freq);

		// tidy up!
		soundFile.close;

		// calculate rfft size
		rfftsize = (size/2 + 1).asInteger;

		// generate required cosine table
		cosTable = Signal.rfftCosTable(rfftsize);

	// YOUR CODE HERE
	// ---

	// analyze!
	complex = waveform.rfft(cosTable);
	magnitudes = complex.magnitude;
	gains = magnitudes.normalize.ampdb;

	// threshold below minimum dB & discard DC
	gains = gains.drop(1).dbamp.thresh(mindb.dbamp).ampdb.round(1.0);
	gains = gains[Array.series(numHarms)];
	gains;
	// ---
})
