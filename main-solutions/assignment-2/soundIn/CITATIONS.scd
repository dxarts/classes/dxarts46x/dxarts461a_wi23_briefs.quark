/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

*/

/*
Attribution:

Bassoon.A3.aiff

Bassoon
Performer: Max Wharton
Technicians: Matt Hallaron, Scott Adamson
http://theremin.music.uiowa.edu/sound%20files/MIS%20Pitches%20-%202014/Woodwinds/Bassoon/Bassoon.ff.A3.stereo.aif
License: UNKNOWN

See also:
http://theremin.music.uiowa.edu/MIS-Pitches-2012/MISBassoon2012.html
http://theremin.music.uiowa.edu/MIS.html
*/