/*
Title: assignment-1

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
---------------------
===== PROBLEM 4 =====
---------------------
Complete the following synthdef so that it meets the following requirements..

1) Returns a stereo panned sine oscillator

2) Uses and correctly maps these arguments:
    dur: duration of note
    gain: gain of note (in dB)
	ris: amount of time (fraction of duration) to get to full volume
    dec: amount of time (fraction of duration) to get to -inf db
    freq: frequency of SinOsc, in Hz
    panAngle: panning angle, in degrees (45 -> left, 0 middle, -45 -> right)

3) Amplitude is modulated (enveloped) by Env.linen

OUTPUT ->

PROBLEM 4
---------

start time 0
nextOSCPacket 0
nextOSCPacket 0
nextOSCPacket 2
nextOSCPacket 2.2
nextOSCPacket 2.2
problem-4 successfully rendered
correct!
---------

HINTS:
See 02c. Composer's Toolkit, Iteration, etc...
See 02d. Envelopes
See 02e. Visualization, Panorama, & Acknowledgements
See SinOsc.ar
See EnvGen.kr
*/
({ |main|
	{ |dur = 2, gain = -6, ris = 0.05, dec = 0.9, freq = 220, panAngle = 0|
		var out, env;

		out = SinOsc.ar(freq);

		// YOUR CODE HERE
		// ---

		// amplitude envelope
		env = EnvGen.kr(
			Env.linen(ris, 1.0 - (ris + dec), dec)
			levelScale: gain.dbamp,  // convert gain db to linear amplitude
			timeScale: dur
		);

		// apply envelope
		out = out * env;

		// pan signal
		out = main.functions[\sinCosPanLaw].value(panAngle) * out;
		// ---

		Out.ar(0, out)
	}
})
