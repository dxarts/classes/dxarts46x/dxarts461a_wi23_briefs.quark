/*
Title: assignment-1

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
---------------------
===== PROBLEM 2b =====
---------------------
calcHarmonicFreqs takes two arguments:
    frequency: fundamental frequency in Hertz
    numHarms: number of harmonics

Complete calcHarmonicFreqs so that when evaluated it returns an array of the first "numHarms" amount of the harmonic spectrum with a fundamental frequency = to the given "frequency"

OUTPUT ->

PROBLEM 2b
---------------
frequency -> 440
numHarms -> 5
harmonics -> [440, 880, 1320, 1760, 2200]
---------------

HINTS:
harmonics are positive integer multiples of the fundamental frequency

fundamental: 440
harmonic1: 440 * 1 = 440
harmonic2: 440 * 2 = 880
harmonic3: 440 * 3 = 1320
etc...

You can use Array: *fill, as in the following example:
    Array.fill(numHarms, {|i| ...behavior});

See also: 02b. Acoustic Calculations, etc.

PLEASE NOTE:
You just need to return an array of harmonics. You do not need to post anything! Posting is done in assignment-1.scd.
*/
({ |frequency, numHarms|

	// YOUR CODE HERE
	// ---
	Array.fill(numHarms, {arg i; frequency * (i+1)});

	// ---
})
