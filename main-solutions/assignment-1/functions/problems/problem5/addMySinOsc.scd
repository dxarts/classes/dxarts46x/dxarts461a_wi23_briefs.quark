/*
Title: assignment-1

Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
---------------------
===== PROBLEM 5 =====
---------------------
Write a function to add a \mySinOsc note event to the score, given the following arguments:
    main: Environment containing global variables, functions, and synthDefs
        (you will use this to access your score)
    start: when note starts (in seconds)
    dur: duration of note
    gain: gain of note (in dB)
    ris: amount of time (fraction of duration) to get to full volume
    dec: amount of time (fraction of duration) to get to -inf db
    freq: frequency of SinOsc, in Hz
    panAngle: panning angle, in degrees (45 -> left, 0 middle, -45 -> right)


OUTPUT ->

PROBLEM 5
---------

start time 0
nextOSCPacket 0
nextOSCPacket 0
nextOSCPacket 2
nextOSCPacket 2.2
nextOSCPacket 2.2
problem-5 successfully rendered
correct!
---------

HINTS:
Score can be accessed using the following syntax -> main.score
Your synth def can accessed using the following syntax -> main.synthDefs[\mySinOsc]

Review:

02c-composers-toolkit-iteration-etc/

____01-ctk-and-the-main-paradigm


PLEASE NOTE:
Score creation and rendering is done in assignment-1.scd.
You do not need to create or render a score in this function
*/

({ |main, start = 0, dur = 0.5, gain = -10, ris = 0.1, dec = 0.8, freq = 220, panAngle = 0|

	// YOUR CODE HERE
	// ---
	main.score.add(
		main.synthDefs[\mySinOsc].note(
			starttime: start,
			duration: dur
		)
		.dur_(dur)
		.gain_(gain)
		.ris_(ris)
		.dec_(dec)
		.freq_(freq)
		.panAngle_(panAngle)
	);
	// ---
})
