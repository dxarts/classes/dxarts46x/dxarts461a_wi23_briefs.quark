/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3

Correct freqs for problem 1
*/

({
	[ 75, 100, 125, 175, 200, 225, 275, 300, 325 ]
})
