/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
Create a tom tom sound additively using amSh2Drum and the following arguments

main: object containing project information
freq: fundamental frequency of tom
start: starttime of drum
dur: duration of drum
gain: volume of drum in db
ris: amount of time drum takes to reach full volume (fraction of duration)
curve: curve of amplitude envelope
ratios: array of ratios for noise bands

Gains, panAngle, as along q and modIndex envelope parameters for each noise band generated should be randomized to taste. Your tom tom should go from noisy to sinusoidal.

*/

({ |main, freq = 880, start = 0, dur = 1, gain = -6, ris = 0.01, curve - 6, ratios = ([0.56, 1, 1.57, 2.1, 1.32])|

	// YOUR CODE HERE
	// ---
	ratios.do({ |thisRatio|
		main.score.add(
			main.synthDefs[\amSh2Drum].note(
				starttime: start,
				duration: dur
			)
			.dur_(dur)
			.freq_(freq * thisRatio)
			.gain_(gain + (rrand(-20, -10)))
			.modIndexStart_(rrand(1, 15))
			.modIndexEnd_(rrand(7, 9))
			.modIndexCurve_(rrand(-6, -3))
			.qStart_(rrand(0.001, 0.005))
			.qEnd_(rrand(0.1, 0.5))
			.qCurve_(rrand(-15, -11))
			.ampRis_(ris)
			.ampCurve_(curve)
			.panAngle_(rrand(-90, 90))
		)
	})
	// ---
})
