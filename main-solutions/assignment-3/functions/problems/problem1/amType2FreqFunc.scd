/*
Course: DXARTS 461a Winter 2023 (https://canvas.uw.edu/courses/1612218)
Authors: Joseph Anderson, Dan Peterson, James Wenlock, et al.
Affiliation: DXARTS, University of Washington (https://dxarts.washington.edu/)
License: GPLv3
*/

/*
----------------------
===== PROBLEM 1 =====
----------------------

Predict the spectral components of type 2 AM synthesis using the following arguments:

freq: the frequency of complex carrier
numCarHarms: number of harmonics present in carrier
modFreq : frequency of modulator (a sine tone)

The output should be an array containing the frequencies of the resulting spectral components

Partials should be returned in order from least to greatest, positive, and in one array. You do not need to remove duplicates

EXPECTED OUTPUT
---------------

PROBLEM 1
---------
[ 75, 100, 125, 175, 200, 225, 275, 300, 325 ]
correct!
---------

HINTS:
See 04a. Amplitude Modulation -> Type 2: predicting spectral components

*/

({ |freqCar = 440, numCarHarms = 5, freqMod = 100|

	// YOUR CODE HERE
	// ---
	var freqsCar;
	var amFreqFunc;

	amFreqFunc = { |freqCar, freqMod|
		var freqSum, freqDiff;

		freqDiff = freqCar - freqMod;
		freqSum = freqCar + freqMod;

		Array.with(
			freqCar,
			freqDiff,
			freqSum
		).sort({ |a, b| a.abs < b.abs })  // sort by absolute value
	};

	// harmonic series - carrier is "complex"
	freqsCar = Array.series(numCarHarms, freqCar, freqCar);

	// find freqs!
	freqsCar.collect({ |freq|
		amFreqFunc.value(freq, freqMod)  // evaluate Type 1 AM, ~amFreqFunc, from above
	}).flatten.sort({ |a, b| a.abs < b.abs })  // sort by absolute value
	// ---
})
